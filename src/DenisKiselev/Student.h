#pragma once

#include <string>
#include "Group.h"

using namespace std;

class Group;

class Student {
private:
	int id;
	string fio;
	Group *gr;
	int *marks;
	int number;
public:
	Student();
	Student(int id, string fio);
	~Student();

	int getId() const;
	string getFio() const;
	Group* getGroup() const;
	void info();
	void toGroup(Group *st);
	void addMark(int m);
	float averageMark();
};