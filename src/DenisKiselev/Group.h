#pragma once

#include <string>
#include "Student.h"

using namespace std;

class Student;

class Group {
private:
	string title;
	Student **st;
	int number;
	Student *head;
public:
	Group();
	Group(string title);
	~Group();

	void addStudent(Student *s);
	bool setRandHead();
	bool searchStudent(string fio);
	bool searchStudent(int id);
	float averageGrMark();
	bool delStudent(int id);
	void info();
	string getTitle() const;
};
